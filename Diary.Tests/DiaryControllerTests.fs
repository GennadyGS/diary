﻿namespace Diary.Tests

open System.Web.Http
open Xunit
open Diary.Api.Controllers
open Diary.Api.Data
open Diary.Api.Models
open Diary.Api.Dto
open System
open System.Web.Http.Results

type DiaryControllerTests() = 
    let sut = new DiaryController (new DiaryRepository ())
    
    [<Fact>]
    let SutIsController() = Assert.IsAssignableFrom<ApiController> sut
    
    [<Fact>]
    let GetReturnsCorrectResult() = 
        let result = sut.Get()
        Assert.IsType<Note[]>(result)
    
    [<Fact>]
    let PostReturnsCorrectResult() = 
        let eventNoteVm = { Date = DateTime.Now; Name = "My Note" }
        let result = sut.Post eventNoteVm
        match result with
            | :? CreatedAtRouteNegotiatedContentResult<AddNoteCommand> as createdResult -> 
                Assert.Equal (eventNoteVm, createdResult.Content)
            | _ -> 
                Assert.True (false, "Invalid result type")

        
