namespace Diary.WebHost

open Owin
open System.Web.Http
open Castle.Windsor
open Castle.MicroKernel.Registration
open WebApiContrib.IoC.CastleWindsor
open Diary.Api.Data
open Diary.Api.Controllers

type private DependencyInstaller() = 
    interface IWindsorInstaller with
        member this.Install(container : IWindsorContainer, _) = 
            container.Register(
                Component
                    .For<IDiaryRepository>()
                    .ImplementedBy<DiaryRepository>()
                    .LifeStyle.PerWebRequest) |> ignore

type private WindsorWebApiControllersInstaller() = 
    interface IWindsorInstaller with
        member this.Install(container : IWindsorContainer, _) : unit = 
            container.Register(
                Classes
                    .FromAssemblyContaining<DiaryController>()
                    .BasedOn<ApiController>()
                    .LifestylePerWebRequest()) |> ignore

[<Sealed>]
type Startup() = 
    
    static member RegisterWebApi(config : HttpConfiguration) = 
        // Configure routing
        config.MapHttpAttributeRoutes()
        // Configure serialization
        config.Formatters.XmlFormatter.UseXmlSerializer <- true
        config.Formatters.JsonFormatter.SerializerSettings.ContractResolver <- Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver
                                                                                   ()
        let container = 
            (new WindsorContainer()).Install(new DependencyInstaller(), new WindsorWebApiControllersInstaller())
        config.DependencyResolver <- new WindsorResolver(container)
    
    // Additional Web API settings
    member __.Configuration(builder : IAppBuilder) = 
        let config = new HttpConfiguration()
        Startup.RegisterWebApi(config)
        builder.UseWebApi(config) |> ignore
