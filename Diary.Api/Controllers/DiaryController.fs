﻿namespace Diary.Api.Controllers

open System.Web.Http
open Diary.Api
open Diary.Api.Dto
open Diary.Api.Data

/// Retrieves values.
[<RoutePrefix("api/diary")>]
type DiaryController (diaryRepository : IDiaryRepository) = 
    inherit ApiController ()
    
    /// Gets all values.
    [<Route("")>]
    member this.Get() = diaryRepository.GetEventNotes() |> Seq.toArray
    
    /// Gets a single value at the specified index.
    [<Route("{id}", Name = Consts.GetEventNoteRouteName)>]
    member this.Get(id : int) : IHttpActionResult = 
        let values = diaryRepository.GetEventNotes() |> Seq.toArray
        if id >= 0 && id < values.Length then base.Ok(values.[id]) :> IHttpActionResult
        else base.NotFound() :> IHttpActionResult
    
    [<Route("")>]
    member this.Post(eventNoteVm : AddNoteCommand) : IHttpActionResult = 
        let id = diaryRepository.InsertEventNote eventNoteVm
        let routeValues = dict [ ("id", id :> obj) ]
        base.CreatedAtRoute(Consts.GetEventNoteRouteName, routeValues, eventNoteVm) :> IHttpActionResult
