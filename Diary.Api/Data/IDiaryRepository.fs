﻿namespace Diary.Api.Data

open Diary.Api.Dto

type IDiaryRepository = 
    abstract GetEventNotes : unit -> seq<Note>

    abstract InsertEventNote : AddNoteCommand -> int
