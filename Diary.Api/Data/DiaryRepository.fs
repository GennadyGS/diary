﻿namespace Diary.Api.Data

open Diary.Api.Dto

type DiaryRepository() = 
    static let mutable values = []

    interface IDiaryRepository with
        member this.GetEventNotes() = List.toSeq values

        member this.InsertEventNote eventNote = 
            let id = List.length values - 1
            let note = { 
                Id = id; 
                Date = eventNote.Date; 
                Name = eventNote.Name; 
            }
            values <- note :: values
            id
            
