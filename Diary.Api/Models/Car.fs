namespace Diary.Api.Models

[<CLIMutable>]
type Car =
    {   
        Make : string
        Model : string 
    }

