﻿namespace Diary.Api.Dto

open System

type AddNoteCommand = 
    {
        Date: DateTime;
        Name: string
    }