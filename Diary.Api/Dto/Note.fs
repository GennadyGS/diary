﻿namespace Diary.Api.Dto

open System

type Note =
    {
        Id: int;
        Date: DateTime;
        Name: string
    }

